﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEditor;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private float speed;
    private WaitForSeconds lifetime = new WaitForSeconds(16f);

    private IEnumerator Lifetime()
    {      
        yield return lifetime;
        Destroy(gameObject);
        yield break;
    }

    private void Update()
    {
        StartCoroutine(Lifetime());
        _rigidbody.transform.Translate(speed/2 * Time.deltaTime, -speed * Time.deltaTime, 0);        
    }
}
