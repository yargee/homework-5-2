﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.Events;

public class Spawn : MonoBehaviour
{
    [SerializeField] private Enemy _enemy;          
    private bool _cooldown = true;    
    private WaitForSeconds  _spawnCooldown = new WaitForSeconds(2f);
    private float[,] _spawnCoordinates;

    private IEnumerator SpawnCooldown()
    {
        yield return _spawnCooldown;
        _cooldown = true;                  
        yield break;
    }

    private void Start()
    {
        SpawnPoint[] _spawnPoints = FindObjectsOfType<SpawnPoint>();
        _spawnCoordinates = new float[_spawnPoints.Length, 2];

        for (int i = 0; i < _spawnPoints.Length; i++)
        {
            _spawnCoordinates[i, 0] = _spawnPoints[i].transform.position.x;
            _spawnCoordinates[i, 1] = _spawnPoints[i].transform.position.y;
        }
    }  

    private void Update()
    {
        if(_cooldown)
        {
            _cooldown = false;
            int _index = Random.Range(0, 5);
            Instantiate(_enemy, new Vector3(_spawnCoordinates[_index, 0], _spawnCoordinates[_index, 1], 0), Quaternion.identity);
            StartCoroutine(SpawnCooldown());     
        }
    }
}
